package com.dung.phan.order.config;

import com.dung.phan.order.interceptor.TrackingRequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TrackingInterceptor implements WebMvcConfigurer {

    @Autowired
    private TrackingRequestInterceptor trackingRequestInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(trackingRequestInterceptor).addPathPatterns("/**");
    }
}
