package com.dung.phan.audit.message;

import com.dung.phan.audit.config.ConcurrencyConfig;
import com.dung.phan.audit.model.AuditData;
import com.dung.phan.audit.service.AuditService;
import com.dung.phan.audit.util.GsonUtil;
import com.google.gson.JsonObject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Service
@Slf4j
public class Consumer {
    @Qualifier(ConcurrencyConfig.TASK_EXECUTOR)
    @Autowired
    @Setter
    private Executor executor;

    @Autowired
    private AuditService auditService;

    @KafkaListener(topics = "audit-service-topic")
    public void consume(String message) {
        log.info("Auditor Service received request with data = {}",message);
        JsonObject jsonBody = GsonUtil.singletonGson().fromJson(message, JsonObject.class);
        CompletableFuture.runAsync(()-> processing(jsonBody)).exceptionally(ex -> {
            log.error(" could not save");
            return null;
        } ) ;
    }
    private void processing(JsonObject data) {
        AuditData auditData = AuditData.builder()
                .data(data.toString())
                .build();
        auditService.saveData(auditData);

    }

}