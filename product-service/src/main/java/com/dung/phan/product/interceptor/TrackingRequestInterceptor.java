package com.dung.phan.product.interceptor;


import com.dung.phan.product.interceptor.dto.HttpExchange;
import com.dung.phan.product.message.AuditProduceMessage;
import com.dung.phan.product.utils.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
public class TrackingRequestInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private AuditProduceMessage auditProduceMessage;
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        CompletableFuture.runAsync(()->doAudit(request));
    }
    private void doAudit(HttpServletRequest request){
        try{
            MyRequestWrapper myRequestWrapper = new MyRequestWrapper(request);
            String body = myRequestWrapper.getBody();
            HttpExchange dto = HttpExchange.builder()
                    .url(request.getRequestURI())
                    .method(request.getMethod())
                    .dateTimeStamp(new Date().getTime())
                    .body(body)
                    .build();
           auditProduceMessage.sendKafkaMessage(GsonUtil.singletonGson().toJson(dto));
        } catch (Exception ex) {
            log.error(" could not audit with error {}",ex.getMessage());
        }

    }
}